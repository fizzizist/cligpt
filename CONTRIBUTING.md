This software is completely free and open source. I welcome any and all contributions. All merge requests should be linked to an issue. If you want to open an issue for someone else to work on, please be very descriptive and clear. If you open an issue that you intend to submit an MR for, then please assign yourself to it, or at least leave a comment about your intention. 

All python code in this repo is autoformatted with Black, and isort.
